document.addEventListener("DOMContentLoaded", function(event) {
let highscoreTable = [];
const win = document.querySelector("#win");
function buildQuiz() { 
  cardEvent.forEach((card) => {
    if (card.classList.contains("is-flipped")) {
        card.classList.remove("is-flipped") ;    
    }
  })
  let ladebildschirm = document.querySelector("#ladebildschirm");
  let container = document.querySelectorAll(".memoryCardBox");
  container.forEach((cont) => {
  cont.classList.add("hidden");
  })
  ladebildschirm.classList.remove("hidden");
  ladebildschirm.classList.add("laden");
  win.classList.add("hidden");
  setTimeout(testfunc, 1900); 
function testfunc(){
  ladebildschirm.classList.remove("laden");
  ladebildschirm.classList.add("hidden");
  container.forEach((cont) => {
  cont.classList.remove("hidden");
  })
  neustart.classList.add("hidden");
  let deletehtml = document.getElementById("versuche");
  start = document.querySelector("#start");
  start.classList.remove("hidden");
  deletehtml.innerHTML = "";
  let numbersUsed = [];
  const doubleCards = 2;
  const pictures = {
    pictureDefault: "memorybilder/default.jpg",
    gamePictures: {
    picture1: "memorybilder/bild1.jpg",
    picture2: "memorybilder/bild2.jpg",
    picture3: "memorybilder/bild3.jpg",
    picture4: "memorybilder/bild4.jpg",
    picture5: "memorybilder/bild5.jpg",
    picture6: "memorybilder/bild6.jpg",
    picture7: "memorybilder/bild7.jpg",
    picture8: "memorybilder/bild8.jpg",
    picture9: "memorybilder/bild9.jpg",
    picture10: "memorybilder/bild10.jpg"
    }
  }

  let count = 1;
  let counter = 0;
  for(let i = 0; i < doubleCards; i++) {
    for(picture in pictures.gamePictures) {
      let output = [];
      output.push(` <div class = "cardFace"><img data-property = "${picture}" class = "pictures" src = "${pictures.pictureDefault}"></div>`);
      let cardNumber = Math.floor(Math.random() * 20) + 1; 
      if (counter >=1) {
        if (numbersUsed.includes(cardNumber));
          do {
            cardNumber = Math.floor(Math.random() * 20) + 1; 
            }
            while( numbersUsed.includes(cardNumber)); 
            }
            counter++;
            count++
            numbersUsed.push(cardNumber);
            let card = document.getElementById(cardNumber);
            output.push(` <div class = " cardFace cardFaceBack"><img data-property = "${picture}" class = "pictures" src = "${pictures.gamePictures[picture]}"></div>`);
            card.innerHTML = output.join(""); 
    }
  }
}
}

let icon;
let parent1;
let parent2;
let property1;
let property2;
let cardCounter = 0;
let versuche = 0;
let highscore;
let cardsFlipped = 0;
let names = document.querySelector("#bestenliste");
function flipCard(e) {
  if (gestartet === true) {
  if (this.classList.contains("is-flipped")) {
    return;
  }
  if (cardCounter === 2) {
    return;
  }
  
  let cardClass = this;
  let child = cardClass.querySelector(".cardFace");
  let img = child.querySelector(".pictures");
  cardClass.classList.add("is-flipped");

  cardCounter++;
    if (cardCounter === 1) {
      property1 = img;
      parent1 = this;
        
    }
    if(cardCounter === 2) {
      parent2 = this;
      property2 = img;
      versuche ++;
      versuchNr.innerHTML = `Versuche:${versuche} `;
      setTimeout(checkProps, 1200);
      function checkProps() {
        if(property1.dataset.property === property2.dataset.property) {
          cardCounter = 0;
          property1 ="";
          property2 ="";
          cardsFlipped +=2;
            if (cardsFlipped === 20){
              gestartet = false;
              names.innerHTML ="";
              highscore = {Name:name+"&#"+icon, Versuche:versuche};
              highscoreTable.push(highscore);
              
              highscoreTable.sort(function(a, b) {
                return (a.Versuche - b.Versuche);
                    
              });
              if (highscoreTable.length > 10) {
              highscoreTable.pop(highscoreTable[10]);
              }
              
              highscoreTable.forEach((score,) => {
                names = document.querySelector("#bestenliste");
                let nameOutput = document.createElement("div");
                nameOutput.classList.add("center");
                nameOutput.innerHTML = `${score.Name} Versuche:${score.Versuche}`;
                names.appendChild(nameOutput);
              })
                win.innerHTML = `Gut gemacht ${name}!<br>Du hast ${versuche} Versuche gebraucht.`
                win.classList.add("winner");
                win.classList.remove("hidden");
                let container = document.querySelectorAll(".memoryCardBox");
                container.forEach((cont) => {
                cont.classList.add("hidden");
                })
            } 
        }
        else {
          cardCounter = 0;
          parent1.classList.remove("is-flipped");
          parent2.classList.remove("is-flipped");
          property1 ="";
          property2 ="";
          }
      }
    }        
  }
}

let name;
let versuchNr;
let gestartet = false;
function startGame() {
  versuche = 0;
  cardsFlipped = 0;
  icon = mySelect.value;
  name = document.getElementById("name").value;
  versuchNr = document.getElementById("versuche");
  versuchNr.innerHTML = `Versuche:${versuche} `;
  gestartet = true;
  start.classList.add("hidden");
  neustart.classList.remove("hidden");
}

let mySelect = document.getElementById('mySelect');
let newOption;
let emojRange = [128512, 128580];
for (let x = emojRange[0]; x < emojRange[1]; x++) {
  newOption = document.createElement('option');
  newOption.value = x;
  newOption.innerHTML = "&#" + x + ";";
  mySelect.appendChild(newOption);
}

  const option = document.querySelector("#placeholder");
  function hidePlaceholder() {
    option.classList.add("hidden");
  }

  const cardEvent = document.querySelectorAll(".memoryCard");
  mySelect.addEventListener('click', hidePlaceholder);
  let start = document.getElementById("start");
  let neustart = document.getElementById("neustart");
  neustart.addEventListener('click', buildQuiz);
  start.addEventListener('click', startGame);
  cardEvent.forEach((card) => {
  card.addEventListener('click', flipCard);
  })
  buildQuiz();
  })
